<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();
class Model_Auction extends Model
{
	function complete_auction($id) {
		global $last_sql_err;
		
		$id = (int)$id;
		$ar = array("id"=>$id);
		$sql = "update auctions t set t.status_id = 3 where t.id = :id";
		//обновляем статус на "Завершенный"
		prepare_query($sql,$ar);
		
		if(!empty($last_sql_err)) 
			return false;
		
		$auction = fetch_row(prepare_query("select t.name from auctions t where t.id = :id",$ar));
		
		
		$sql_win_user = "select r.user_id from rates r where r.auction_id = :id and r.dt = (select max(r2.dt) from rates r2 where r2.auction_id = r.auction_id) ";
		$win_user_id = fetch_row(prepare_query($sql_win_user,$ar));
		$win_user_id = $win_user_id['user_id'];
		
		
		
		
		$sql_all_users = "select distinct u.mail, u.id  from rates r inner join users u on u.id = r.user_id where r.auction_id = :id";
		
		$users = fetch_all_rows(prepare_query($sql_all_users,$ar));
		
		$admins =  fetch_all_rows(query("select t.id, t.mail, 1 as is_admin from users t where 0 < (select count(*) from users2roles ur where ur.user_id = t.id)"));
		
		$users = array_merge($users, $admins);
		
		
		foreach($users as $item) {
			$message = "Здравствуйте Вы участвовали в аукционе '".$auction['name']."', он был завершен, к сожалению Вы не являетесь победителем";
			if($item['id'] == $win_user_id)
				$message = "Здравствуйте Вы участвовали в аукционе '".$auction['name']."', он был завершен, и Вы стали победителем!!!";
			
			if($item['is_admin']) {
				$message = "Завершился аукцион '".$auction['name']."' id = ".$id;
			}
			
			$to = $item['mail'];
			$subject = 'auction';
			$headers = 'From: webmaster@example.com' . "\r\n" .
				'Reply-To: webmaster@example.com' . "\r\n";
			
			mail($to, $subject, $message, $headers);
		}
		
		return true;
		
	}
}
