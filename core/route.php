<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();
/*
Класс-маршрутизатор для определения запрашиваемой страницы.
> цепляет классы контроллеров и моделей;
> создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
*/
class Route
{

	static function start()
	{
		
		// контроллер и действие по умолчанию
		$controller_name = 'Auction';
		$action_name = 'index';
		
		
		// получаем имя контроллера
		if ( !empty($_GET['module']) )
		{	
			$controller_name = $_GET['module'];
		}
		
		// получаем имя экшена
		if ( !empty($_GET['sub_module']) )
		{
			$action_name = $_GET['sub_module'];
		}
		
		if(!isset($_SESSION['user']) && $action_name != 'login' ) {
			$controller_name = 'Auth';
			$action_name = 'index';
		}
		
		
		if($controller_name == 'admin' && !$_SESSION['user']['roles'][1])
			Route::ErrorPage404();
		
		

		// добавляем префиксы
		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		
		// подцепляем файл с классом модели (файла модели может и не быть)
		$model_file = strtolower($model_name).'.php';
		$model_path = "models/".$model_file;
		if(file_exists($model_path))
		{
			include "models/".$model_file;
		}
		
		// подцепляем файл с классом контроллера
		$controller_file = strtolower($controller_name).'.php';
		$controller_path = "controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			
			include "controllers/".$controller_file;
		}
		else
		{	
			Route::ErrorPage404();
		}
		
		// создаем контроллер
		$controller_name = ucfirst($controller_name);
		$controller = new $controller_name;
		$action = $action_name;
		
		
		
		if(method_exists($controller, $action))
		{
			// вызываем действие контроллера
			$controller->$action();
		}
		else
		{
			Route::ErrorPage404();
		}
	
	}

	static function ErrorPage404()
	{
		$view = new View();
		setMessage('У Вас нет прав на выполнение данного действия.<br>Обратитесь к администратору системы','err');
		$view->generate(null, 'template_view.php',array());
		exit;
	}
    
}
