<!--Для всплывающего окна-->
<link rel="stylesheet" href="/css/jquery.thicktask.css" type="text/css" media="screen">
<script type="text/javascript" src="/js/jquery.thicktask.js"></script>


<style>

#list_auctions td:last-child{
	text-align:center;
}

#list_auctions .img{
	max-width:100px;
	max-height:100px;
	cursor:pointer;
}

</style>
<script>
$( document ).ready(function() {
	
	get_list_auctions();
		
});

var current_ajax_list = null;

function get_list_auctions(){
	
	if(current_ajax_list)
	current_ajax_list.abort();
	

	current_ajax_list = $.ajax({
		type: 'GET',
		url: '/scripts/auction/get_list_auctions.php?',
		dataType: 'html',
		beforeSend: function(){
			$("#list_auctions").html("<tr><td colspan='20' align='center'><img src='/images/load.gif'/></td></tr>");
		},
		success: function(data) {
			$("#list_auctions").html(data);
			
			
		},
		complete:function() {
			
		}
	
	});		
}	



</script>

<h1>
	Аукционы
</h1>


<div style="margin-top:10px;" class="wrap_table">
	
	<table class="data_table ">
		<thead>
			<tr>
				
				
				
				<th style="width:150px;" >Название</th>
				<th>Описание</th>
				<th style="width:50px;">Изображение</th>
				<th style="width:150px;">величина одной ставки</th>
				<th style="width:150px;">цена моментального выигрыша</th>
				<th style="width:100px;">Дата окончания</th>
				<th style="width:100px;"></th>
				
			</tr>
		</thead>

			<tbody id="list_auctions"></tbody>
		</table>
	
</div>


