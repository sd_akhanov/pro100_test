<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();
class Model_Admin extends Model
{
	function delete_user($id) {
		global $last_sql_err;
		
		$id = (int)$id;
		$sql = "delete from users where id = {$id} ";
		query($sql);	
		
		if(empty($last_sql_err)) 
			return true;
		return false;
	}
	
	function get_auction_by_id($id){
		$id = (int)$id;
		
		$sql = "select t.*, DATE_FORMAT(t.dt_end,'%d.%m.%Y') as dt_end from auctions t where t.id = {$id} ";
		
		$auction = fetch_row(query($sql));
		
		return $auction;
	}
		
		
	function add_auction($data){
		
		global $last_sql_err;
		
		if(!$_FILES['file']['size'])
			return false;
		
		$type_file = explode(".",$_FILES['file']['name']);
		if(count($type_file) > 1)
			$type_file = $type_file[count($type_file)-1];
		else	
			$type_file = '';
			
		
		
		$new_id = fetch_row(query("SHOW TABLE STATUS WHERE Name = 'auctions'"));
		$new_id = $new_id['Auto_increment'];
		
		
		
		$uploaddir = $_SERVER['DOCUMENT_ROOT']."/files/auctions/";
		$uploadfile = $uploaddir.$new_id.".".$type_file;
		
		
		move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
		
		$ar = array(
			"id"=>$new_id,
			"status_id"=>$data['status_id'],
			"name"=>$data['name'],
			"description"=>$data['description'],
			"img"=>$new_id.".".$type_file,
			"rate_magnitude"=>$data['rate_magnitude'],
			"price_instant_win"=>$data['price_instant_win'],
			"dt_end"=>$data['dt_end'],
		);
		
		$sql = "insert into auctions (id, status_id, name, description, img, rate_magnitude, price_instant_win, dt_end) values (:id, :status_id, :name, :description, :img, :rate_magnitude, :price_instant_win, STR_TO_DATE(:dt_end, '%d.%m.%Y'))";
		
		prepare_query($sql,$ar);
		
		if(!empty($last_sql_err))
			return false;
		
		return	true;	
	}	
	
	function update_auction($data) {
		$auction = $this->get_auction_by_id($data['id']);
		if($auction['status_id'] == 3)
			return false;
		
		global $last_sql_err;
		
		if($_FILES['file']['size']) {
			$type_file = explode(".",$_FILES['file']['name']);
			if(count($type_file) > 1)
				$type_file = $type_file[count($type_file)-1];
			else	
				$type_file = '';
		
			$uploaddir = $_SERVER['DOCUMENT_ROOT']."/files/auctions/";
			$uploadfile = $uploaddir.$data['id'].".".$type_file;
			
			move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
		}
		
		$ar = array(
			"id"=>$data['id'],
			"status_id"=>$data['status_id'],
			"name"=>$data['name'],
			"description"=>$data['description'],
			"rate_magnitude"=>$data['rate_magnitude'],
			"price_instant_win"=>$data['price_instant_win'],
			"dt_end"=>$data['dt_end'],
		);	
		
		
		$sql = "update auctions t 
							set t.status_id = :status_id,
								t.name = :name,
								t.description = :description,
								t.rate_magnitude = :rate_magnitude,
								t.price_instant_win = :price_instant_win,
								t.dt_end = STR_TO_DATE(:dt_end, '%d.%m.%Y') where t.id = :id";
		
		prepare_query($sql,$ar);
		
		if(!empty($last_sql_err))
			return false;
		
		return	true;
		
	}
	
	function delete_auction($id) {
		$id = (int)$id;
		
		$sql = "delete from auctions where id = {$id} ";
		query($sql);	
		
		if(empty($last_sql_err)) 
			return true;
		return false;
	}
	
	function get_auction_status(){
		$status = fetch_all_rows(query("select t.* from auction_status t order by t.id"));
		
		return $status;
	}
}
