/*
 * Thickbox 3.1 - One Box To Rule Them All.
 * By Cody Lindley (http://www.codylindley.com)
 * Copyright (c) 2007 cody lindley
 * Licensed under the MIT License: http://www.opensource.org/licenses/mit-license.php
*/
$.browser.msie6 =
$.browser.msie
&& /MSIE 6\.0/i.test(window.navigator.userAgent)
&& !/MSIE 7\.0/i.test(window.navigator.userAgent);
var TA_pathToImage = "/images/loading.gif";

/*!!!!!!!!!!!!!!!!! edit below this line at your own risk !!!!!!!!!!!!!!!!!!!!!!!*/

//on page load call TA_init
$(document).ready(function(){
	TA_init('a.thicktask, area.thicktask, input.thicktask');//pass where to apply thickbox
	imgLoader = new Image();// preload image
	imgLoader.src = TA_pathToImage;
});

//add thickbox to href & area elements that have a class of .thickbox
function TA_init(domChunk){
	$(domChunk).click(function(){
	var t = this.title || this.name || $(this).attr('tooltip') || null;
	var a = this.href || this.alt;
	var g = this.rel || false;
	TA_show(t,a,g);
	this.blur();
	return false;
	});
}

function TA_show(caption, url, imageGroup) {//function called when the user clicks on a thickbox link
	
	$("body").css({"overflow":"hidden"});

	try {
		if (typeof document.body.style.maxHeight === "undefined") {//if IE 6
			$("body","html").css({height: "100%", width: "100%"});
			$("html").css("overflow","hidden");
			if (document.getElementById("TA_HideSelect") === null) {//iframe to hide select elements in ie6
				$("body").append("<iframe id='TA_HideSelect'></iframe><div id='TA_overlay'></div><div id='TA_window'></div>");
				//$("#TA_overlay").click(TA_remove);
			}
		}else{//all others
			if(document.getElementById("TA_overlay") === null){
				$("body").append("<div id='TA_overlay'></div><div id='TA_window'></div>");
				//$("#TA_overlay").click(TA_remove);
			}
		}

		$("#TA_overlay").height($(document).height());
		$("#TA_overlay").width($(document).width());

		if(TA_detectMacXFF()){
			$("#TA_overlay").addClass("TA_overlayMacFFBGHack");//use png overlay so hide flash
		}else{
			$("#TA_overlay").addClass("TA_overlayBG");//use background and opacity
		}

		if(caption===null){caption="";}
		$("body").append("<div id='TA_load'><img src='"+imgLoader.src+"' /></div>");//add loader to the page
		$('#TA_load').show();//show loader

		var baseURL;
	   if(url.indexOf("?")!==-1){ //ff there is a query string involved
			baseURL = url.substr(0, url.indexOf("?"));
	   }else{
	   		baseURL = url;
	   }

	   var urlString = /\.jpg$|\.jpeg$|\.png$|\.gif$|\.bmp$/;
	   var urlType = baseURL.toLowerCase().match(urlString);

		if(urlType == '.jpg' || urlType == '.jpeg' || urlType == '.png' || urlType == '.gif' || urlType == '.bmp'){//code to show images

			TA_PrevCaption = "";
			TA_PrevURL = "";
			TA_PrevHTML = "";
			TA_NextCaption = "";
			TA_NextURL = "";
			TA_NextHTML = "";
			TA_imageCount = "";
			TA_FoundURL = false;
			if(imageGroup){
				TA_TempArray = $("a[@rel="+imageGroup+"]").get();
				for (TA_Counter = 0; ((TA_Counter < TA_TempArray.length) && (TA_NextHTML === "")); TA_Counter++) {
					var urlTypeTemp = TA_TempArray[TA_Counter].href.toLowerCase().match(urlString);
						if (!(TA_TempArray[TA_Counter].href == url)) {
							if (TA_FoundURL) {
								TA_NextCaption = TA_TempArray[TA_Counter].title;
								TA_NextURL = TA_TempArray[TA_Counter].href;
								TA_NextHTML = "<span id='TA_next'>&nbsp;&nbsp;<a href='#'>Следующая &gt;</a></span>";
							} else {
								TA_PrevCaption = TA_TempArray[TA_Counter].title;
								TA_PrevURL = TA_TempArray[TA_Counter].href;
								TA_PrevHTML = "<span id='TA_prev'>&nbsp;&nbsp;<a href='#'>&lt; Предыдущая</a></span>";
							}
						} else {
							TA_FoundURL = true;
							TA_imageCount = "Фотография " + (TA_Counter + 1) +" из "+ (TA_TempArray.length);
						}
				}
			}

			imgPreloader = new Image();
			imgPreloader.onload = function(){
			imgPreloader.onload = null;

			// Resizing large images - orginal by Christian Montoya edited by me.
			var pagesize = TA_getPageSize();
			var x = pagesize[0] - 150;
			var y = pagesize[1] - 150;
			var imageWidth = imgPreloader.width;
			var imageHeight = imgPreloader.height;
			if (imageWidth > x) {
				imageHeight = imageHeight * (x / imageWidth);
				imageWidth = x;
				if (imageHeight > y) {
					imageWidth = imageWidth * (y / imageHeight);
					imageHeight = y;
				}
			} else if (imageHeight > y) {
				imageWidth = imageWidth * (y / imageHeight);
				imageHeight = y;
				if (imageWidth > x) {
					imageHeight = imageHeight * (x / imageWidth);
					imageWidth = x;
				}
			}
			// End Resizing

			TA_WIDTH = imageWidth + 30;
			TA_HEIGHT = imageHeight + 60;
			$("#TA_window").css("height","450px").append("<a href='' id='TA_ImageOff' title='Close'><img id='TA_Image' src='"+url+"' width='"+imageWidth+"' height='"+imageHeight+"' alt='"+caption+"'/></a>" + "<div id='TA_caption'>"+caption+"<div id='TA_secondLine'>" + TA_imageCount + TA_PrevHTML + TA_NextHTML + "</div></div><div id='TA_closeWindow'><a href='javascript://' id='TA_closeWindowButton' title='Закрыть'>Закрыть</a></div>");

			$("#TA_closeWindowButton").click(TA_remove);

			if (!(TA_PrevHTML === "")) {
				function goPrev(){
					if($(document).unbind("click",goPrev)){$(document).unbind("click",goPrev);}
					$("#TA_window").remove();
					$("body").append("<div id='TA_window'></div>");
					TA_show(TA_PrevCaption, TA_PrevURL, imageGroup);
					return false;
				}
				$("#TA_prev").click(goPrev);
			}

			if (!(TA_NextHTML === "")) {
				function goNext(){
					$("#TA_window").remove();
					$("body").append("<div id='TA_window'></div>");
					TA_show(TA_NextCaption, TA_NextURL, imageGroup);
					return false;
				}
				$("#TA_next").click(goNext);

			}

			document.onkeydown = function(e){
				if (e == null) { // ie
					keycode = event.keyCode;
				} else { // mozilla
					keycode = e.which;
				}
				if(keycode == 27){ // close
					TA_remove();
				} else if(keycode == 190){ // display previous image
					if(!(TA_NextHTML == "")){
						document.onkeydown = "";
						goNext();
					}
				} else if(keycode == 188){ // display next image
					if(!(TA_PrevHTML == "")){
						document.onkeydown = "";
						goPrev();
					}
				}
			};

			TA_position();
			$("#TA_load").remove();
			$("#TA_ImageOff").click(TA_remove);
			$("#TA_window").css({display:"block"}); //for safari using css instead of show
			};

			imgPreloader.src = url;
		}else{//code to show html

			var queryString = url.replace(/^[^\?]+\??/,'');
			var params = TA_parseQuery( queryString );

			TA_WIDTH = (params['width']*1) + 30 || 90; //defaults to 630 if no paramaters were added to URL
			TA_HEIGHT = (params['height']*1) + 40 || 80; //defaults to 440 if no paramaters were added to URL
			ajaxContentW = TA_WIDTH - 30;
			ajaxContentH = TA_HEIGHT - 45;

			if(url.indexOf('TA_iframe') != -1){// either iframe or ajax window
					urlNoQuery = url.split('TA_');
					$("#TA_iframeContent").remove();
					if(params['modal'] != "true"){//iframe no modal
						$("#TA_window").append("<div id='TA_title'><div id='TA_ajaxWindowTitle'><h1 style=padding:0; margin:0;'>"+caption+"</h1></div><div id='TA_closeAjaxWindow'><a href='javascript://' id='TA_closeWindowButton' title='Close'><img src=\"/images/close.png\"></a></div></div><iframe frameborder='0' hspace='0' src='"+urlNoQuery[0]+"' id='TA_iframeContent' name='TA_iframeContent"+Math.round(Math.random()*1000)+"' onload='TA_showIframe()' style='width:"+(ajaxContentW + 29)+"px;height:"+(ajaxContentH + 17)+"px;' > </iframe>");
					}else{//iframe modal
					$("#TA_overlay").unbind();
						$("#TA_window").append("<iframe frameborder='0' hspace='0' src='"+urlNoQuery[0]+"' id='TA_iframeContent' name='TA_iframeContent"+Math.round(Math.random()*1000)+"' onload='TA_showIframe()' style='width:"+(ajaxContentW + 29)+"px;height:"+(ajaxContentH + 17)+"px;'> </iframe>");
					}
			}else{// not an iframe, ajax
					if($("#TA_window").css("display") != "block"){
						if(params['modal'] != "true"){//ajax no modal
						$("#TA_window").append("<div id='TA_title'><div id='TA_ajaxWindowTitle'><h1 style='margin:0; padding:0;'>"+caption+"</h1></div><div id='TA_closeAjaxWindow'><a href='javascript://' id='TA_closeWindowButton' onclick='TA_remove();'><img src=/images/close.png></a></div></div><div id='TA_ajaxContent'></div>");
						}else{//ajax modal
						$("#TA_overlay").unbind();
						$("#TA_window").append("<div id='TA_ajaxContent' class='TA_modal' style='width:"+ajaxContentW+"px;height:"+ajaxContentH+"px;'></div>");
						}
					}else{//this means the window is already up, we are just loading new content via ajax
						//sdg: it does not let squash for divs w/o width def.
						//$("#TA_ajaxContent")[0].style.width = ajaxContentW +"px";
						$("#TA_ajaxContent")[0].style.height = ajaxContentH +"px";
						$("#TA_ajaxContent")[0].scrollTop = 0;
						$("#TA_ajaxWindowTitle").html("<h1 style=padding:0; margin:0;'>"+caption+"</h1>");
					}
			}

//			$("#TA_closeWindowButton").click(TA_remove);
			$("#TA_closeWindowButton").click(function(){
				return ActionConfirm();
			});


				if(url.indexOf('TA_inline') != -1){
					$("#TA_ajaxContent").append($('#' + params['inlineId']).children());
					$("#TA_window").unload(function () {
						$('#' + params['inlineId']).append( $("#TA_ajaxContent").children() ); // move elements back when you're finished
					});
					TA_position();
					$("#TA_load").remove();
					$("#TA_window").css({display:"block"});
				}else if(url.indexOf('TA_iframe') != -1){
					TA_position();
					if($.browser.safari){//safari needs help because it will not fire iframe onload
						$("#TA_load").remove();
						$("#TA_window").css({display:"block"});
					}
				}else{
					$("#TA_ajaxContent").load(url += "&random=" + (new Date().getTime()),function(){//to do a post change this load method
						TA_position();
						$("#TA_load").remove();
						TA_init("#TA_ajaxContent a.thickbox");
						$("#TA_window").css({display:"block"});
					});
				}

				var clientHeight = $(window).height()-100;//600;
				$("#TA_window").height(clientHeight);
				$("#TA_ajaxContent").height(clientHeight-35);

		}

		if(!params['modal']){
			document.onkeyup = function(e){
				if (e == null) { // ie
					keycode = event.keyCode;
				} else { // mozilla
					keycode = e.which;
				}
				if(keycode == 27){ // close
					TA_remove();
				}
			};
		}

	$('#TA_ajaxWindowTitle h1').width($('#TA_window').width()-50);
	} catch(e) {
		//nothing here
	}
}

//helper functions below
function TA_showIframe(){
	$("#TA_load").remove();
	$("#TA_window").css({display:"block"});
}

function TA_remove() {
 	$("#TA_imageOff").unbind("click");
	$("#TA_closeWindowButton").unbind("click");
	$("#TA_window").fadeOut("fast",function(){$('#TA_window,#TA_overlay,#TA_HideSelect').trigger("unload").unbind().remove();});
	$("#TA_load").remove();
	if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
		$("#TA_window").hide();
		$("html").css("overflow","");
		$("body","html").css({height: "100%", width: "auto"});
	}
	document.onkeydown = "";
	document.onkeyup = "";
	
	$("body").css({"overflow":"auto"});

	return false;
}

function TA_position() {
	$("#TA_window").css({width: '90%'});
	if ( !(jQuery.browser.msie6)) { // take away IE6
		$("#TA_window").css({marginTop: '-' + parseInt((TA_HEIGHT/2),10) + 'px'});
	}
}

function TA_parseQuery ( query ) {
   var Params = {};
   if ( ! query ) {return Params;}// return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) {continue;}
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

function TA_getPageSize(){
	var de = document.documentElement;
	var w = window.innerWidth || self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
	var h = window.innerHeight || self.innerHeight || (de&&de.clientHeight) || document.body.clientHeight;
	arrayPageSize = [w,h];
	return arrayPageSize;
}

function TA_detectMacXFF() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox')!=-1) {
    return true;
  }
}


