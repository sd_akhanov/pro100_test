<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();
class Model_Auth extends Model
{
	public function get_user_by_mail($login) {
		
		$sql = "SELECT t.* from users t where t.mail= :login";
		usleep(5000);
		$ar = array("login"=>$login);
		$result  = fetch_row(prepare_query($sql,$ar));
		
		$sql_roles = "select t.role_id from users2roles t where t.user_id = {$result['id']}";
		$temp = query($sql_roles);
		$rights = array();
		if($temp) {
			while($row = fetch_next($temp)) {
				$result['roles'][$row['role_id']] = $row['role_id'];
			}
		}
		
		return $result;
		
	}
	
	function register_user($data) {
		if(mb_strlen($data['password']) < 5)
			return false;
	
		global $last_sql_err;
		
		$sql = "insert into users (mail,pass) values (:mail,:password)";
		
		$ar = array("mail"=>$data['mail'],"password"=>md5($data['password']));
		prepare_query($sql,$ar);
		
		if(!empty($last_sql_err))
			return false;
		
		return true;	
	}
		
}
