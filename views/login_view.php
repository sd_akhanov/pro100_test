<style>
	
.hide{
	display:none;
}
</style>
<h1 style="margin:100px 0 20px 0;text-align:center;">Аукцион</h1>
<div style=" margin:0 auto;  width:390px;">
	
	
	<div style="float:left;width:160px;line-height:21px; text-align:right;padding-right:5px;">E-mail</div><div style="float:left; text-align:right;"><input autocomplete="off" id="mail" style=" width:205px;" type="text"  class="form-control"></div>
	<div style="clear:both;margin-bottom:10px;"></div>
	<div style="float:left;width:160px;line-height:21px;text-align:right;padding-right:5px;">Пароль</div><div style="float:left;text-align:right;"><input autocomplete="off" id="password" style=" width:205px;" type="password" class="form-control"></div>
	<div style="clear:both;margin-bottom:10px;"></div>
	<div style="float:left;width:160px;line-height:21px;text-align:right;padding-right:5px;" class="toggle_block hide">Подтверждение пароля</div><div class="toggle_block hide" style="float:left;text-align:right;"><input autocomplete="off" id="password2" style=" width:205px;" type="password"  class="form-control"></div>
	<div style="clear:both;margin-bottom:10px;"></div>
	<div style="margin-left:165px;">
	
	<input id="btn_login" style="margin-right:6px;" type="button" value="Вход" class="btn btn-default toggle_block" />
	<input  type="button" value="Регистрация" class="btn btn-default toggle_block" onclick="toggle_block()" />
	<input id="btn_reg" style="margin-right:6px;"  type="button" value="Зарегистрироваться" class="btn btn-default toggle_block hide" onclick="register()" />
	<input   type="button" value="Отмена" class="btn btn-default toggle_block hide"  onclick="toggle_block()"/>
	</div>
	

	
</div>

<script>
function toggle_block(){
	$(".toggle_block").toggle();
}

function register(){
	if($("#password").val() != $("#password2").val()) {
		alert('Пароль и подтверждение пароля не совпадают');
		return;
	}
	if($("#password").val().length < 5) {
		alert('Слишком короткий пароль');
		return;
	}	

	var data = {mail:$("#mail").val(),'password':$("#password").val()}
	$.ajax({
		type: 'POST',
		url: '/scripts/auth/register.php',
		data:data,
		dataType: 'json',
		success: function(data) {
			if(data.auth === true){
				document.location = '/';
			}	
			else{
				alert('Ошибка регистрации')	;
			}	
		}
	});
}
	
$("#btn_login").click(function(){
	login();
});

document.onkeyup = function (e) {
    e = e || window.event;
    if (e.keyCode === 13) {
		login();
    }
		
   
    return false;
}

function login(){
	
	var data = {mail:$("#mail").val(),'password':$("#password").val()}
	$.ajax({
		type: 'POST',
		url: '/scripts/auth/login.php',
		data:data,
		dataType: 'json',
		beforeSend: function(){
			
			
		},
		success: function(data) {
			if(data.auth === true){
				document.location = '/';
			}	
			else{
				alert('Не верно указаны логин и/или пароль!')	
			}	
		}
	});
}
</script>	