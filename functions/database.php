<?php
//подключение в бд
function connect() {
	global $mysql_pdo;
	
	$databaseName = 'auction';
	
	$host = 'localhost';
	$user = 'root';
	$password = ''; 
	
	
	$dsn = "mysql:host=".$host.";dbname=".$databaseName.";charset=utf8";
	$opt = array(
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
	);
	try {
		$mysql_pdo = new PDO($dsn, $user, $password, $opt);
	} catch (PDOException $e) {
		
		exit("Критичная ошибка!!! Не удалось подключиться к базе данных");
	}
	query("SET NAMES utf8");   
}


// выполняет запрос 
function query($query) {
	global $mysql_pdo,$last_sql_err;
	$stmt = null;
	try {
		$stmt = $mysql_pdo->query($query);
	} catch (PDOException $e) {
		
		$last_sql_err = $e->getMessage();
	}
	return $stmt;
	
}
//$ar_binds = array('key'=>$val);
function prepare_query($query,$ar_binds) {
	global $mysql_pdo,$last_sql_err;
	$stmt = null;
	try {
		$stmt = $mysql_pdo->prepare($query);
		$stmt->execute($ar_binds);
	} catch (PDOException $e) {
		
		$last_sql_err = $e->getMessage();
	}
	return $stmt;
	
}

// возвращает первую строку запроса
function fetch_row($stmt) {
	$row = array();
	if($stmt)
		$row = $stmt->fetch();
	return $row;
}


function fetch_next($stmt) {
	$row = $stmt->fetch();
	return $row;
}

//возвращает все строки запроса
function fetch_all_rows($stmt) {
	$array = array();
	if($stmt) {
		$array = $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	return $array;
}
