<?php
define("BASEPATH", true);	
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__)."/../../";

session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/functions/config.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/functions/function.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/core/model.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/model_auction.php';

session_write_close();

$sql = "select t.id from auctions t where t.status_id = 1 and t.dt_end = Date(now())";
$auctions = fetch_all_rows(query($sql));

$Model_Auction = new Model_Auction();

foreach($auctions as $item)
	$Model_Auction -> complete_auction($item['id']);