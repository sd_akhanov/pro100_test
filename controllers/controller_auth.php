<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();
class Controller_Auth extends Controller
{
	function __construct()
	{
		$this -> model = new Model_Auth();
		$this -> view = new View();
	}
	function index()
	{	
			
		$this->view->generate('login_view.php', 'template_view.php',array());
	}
	
	
	function login_ajax() {
		$user = $this -> model -> get_user_by_mail($_POST['mail']);
		
		$pass = md5($_POST['password']);
		if($pass == $user['pass']) {
			
			$_SESSION['user'] = $user;
		
			
			return true;
		}
		return false;
		
		
	}	
	
	function register(){
		if($this -> model -> register_user($_POST)) {
			$this -> login_ajax();
			return true;
		}
		else 
			return false;
	}
	
	function logout() {
		unset($_SESSION['user']);
		headerTo("/");
	}
		
}