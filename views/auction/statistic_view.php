<!--Для всплывающего окна-->
<link rel="stylesheet" href="/css/jquery.thicktask.css" type="text/css" media="screen">
<script type="text/javascript" src="/js/jquery.thicktask.js"></script>

<style>

#list_statistic td:last-child{
	text-align:center;
}
#list_statistic .img{
	max-width:100px;
	max-height:100px;
	cursor:pointer;
}
</style>
<script>
$( document ).ready(function() {
	
	get_list_auctions();
	
});

var current_ajax_list = null;

function get_list_auctions(){
	
	if(current_ajax_list)
	current_ajax_list.abort();
	

	current_ajax_list = $.ajax({
		type: 'GET',
		url: '/scripts/auction/get_statistic.php?',
		dataType: 'html',
		beforeSend: function(){
			$("#list_statistic").html("<tr><td colspan='20' align='center'><img src='/images/load.gif'/></td></tr>");
		},
		success: function(data) {
			$("#list_statistic").html(data);
			var summ = 0;
			$(".is_win .max_value").each(function(){
				summ += parseInt($(this).text());
			})
			$("#amount_spent").text(summ);
			
		},
		complete:function() {
			
		}
	
	});		
}	



</script>

<h1>
	Моя статистика 
</h1>




<div style="font-size: 14px;">Сумма потраченных средств: <span id="amount_spent">0</span>р.</div>
<div style="margin-top:10px;" class="wrap_table">
	
	<table class="data_table ">
		<thead>
			<tr>
				
				
				
				<th style="width:150px;" >Название</th>
				<th>Описание</th>
				<th style="width:50px;">Изображение</th>
				<th style="width:150px;">величина одной ставки</th>
				<th style="width:150px;">цена моментального выигрыша</th>
				<th style="width:100px;">Дата окончания</th>
				<th style="width:100px;">Ваша максимальная ставка</th>
				<th style="width:100px;">Являетесь ли победителем</th>
				
			</tr>
		</thead>

			<tbody id="list_statistic"></tbody>
		</table>
	
</div>


