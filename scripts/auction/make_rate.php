<?php
define("BASEPATH", true);	
session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/functions/config.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/functions/function.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/core/model.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/model_auction.php';


session_write_close();

$id = (int)$_GET['id'];

$rate = fetch_row(query("select r.*, DATE_FORMAT(r.dt,'%d.%m.%Y %H:%i:%s') as dt from rates r where r.auction_id = {$id} and r.dt = (select max(r2.dt) from rates r2 where r2.auction_id = r.auction_id)"));

$auction = fetch_row(query("select t.*, DATE_FORMAT(t.dt_end,'%d.%m.%Y') as dt_end from auctions t where t.id = {$id}"));



if(isset($_GET['mode']) && $_GET['mode'] == 'make_rate') {
	global $last_sql_err;
	$error = json_encode(array("error"=>true));
	
	//если у акиона статус не "Активный" выдаем ошибку
	if($auction['status_id'] != 1) {
		echo $error;
		exit;
	}
	
	if($_GET['type'] == 'next_value') {
		//Нельзя перебивать свою ставку
		if($_SESSION['user']['id'] == $rate['user_id']) {
			echo $error;
			exit;
		}	
			
		$value = (int)$auction['rate_magnitude'] + 	(int)$rate['value'];
			
		$sql = "insert into rates (auction_id, user_id, dt, value)
				select {$id}, {$_SESSION['user']['id']}, now(), {$value} ";
		//echo $sql;exit;			
		query($sql);	
	}
	else {
		$sql = "insert into rates (auction_id, user_id, dt, value) values ({$id},{$_SESSION['user']['id']}, now(), {$auction['price_instant_win']}) ";
		query($sql);
		
		if(empty($last_sql_err)) {
			$Model_Auction = new Model_Auction();
			$Model_Auction -> complete_auction($id);
		}
	}
	
	
	if(!empty($last_sql_err)) {
		echo $error;
		exit;
	}
	
	
	echo json_encode(array("error"=>false));
	exit;
}



?>

<style>
button{
	margin-top:5px;
}
</style>
<div style="width:95%;margin: 0 auto;">
	<div style="max-width:50%; float:left; margin-right:20px;"><div>Описание: <?=$auction['description']?></div>
	<div>Величина одной ставки: <?=$auction['rate_magnitude']?></div>
	<div>Цена моментального выигрыша: <?=$auction['price_instant_win']?></div>
	<div>Дата окончания: <?=$auction['dt_end']?></div>
	<div>Сумма последней ставки: <?=$rate['value']?></div>
	<div>Дата последней ставки: <?=$rate['dt']?></div>
	<div>
	<? if($_SESSION['user']['id'] != $rate['user_id']) { ?>
		<button onclick="make_rate('next_value')">Предложить ставку <?=(int)$rate['value'] + $auction['rate_magnitude']?> р.</button>
	<? } else { ?>
		<span style="color:red;">Ваша ставка является последней</span>
	<? } ?>
	</div>
	<div>
	<button onclick="win_auction();">Моментальный выигрыш</button>
	</div>
	</div>
	<img src="/files/auctions/<?=$auction['img']?>" style="float:left;max-width:50%;max-height:300px;"  />
	<div style="clear:both;"></div>
</div>
<script>
function win_auction(){
	if(confirm("Вы действительно хотите выиграть в аукционе, предложив <?=$auction['price_instant_win']?> р. ?")) {
		make_rate('win')
	}
	
}

function make_rate(type){
	$.ajax({
		type: 'GET',
		url: '/scripts/auction/make_rate.php?mode=make_rate&id=<?=$id?>&type='+type,
		dataType: 'json',
		success: function(res) {
			if(!res.error) {
				if(type == 'next_value')
					alert("Ставка сделана");
				else
					alert("Поздравляем Вы выиграли аукцион");
			}
			else {
				alert("Произошла ошибка");
			}
		},
		
	
	});
}

$(document).ready(function(){

})
</script>