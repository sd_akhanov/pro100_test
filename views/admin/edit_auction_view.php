<?
	
?>

<link rel="stylesheet" href="/css/ui/redmond/jquery.ui.theme.css" type="text/css" media="screen">
<link rel="stylesheet" href="/css/ui/redmond/jquery.ui.datepicker.css" type="text/css" media="screen">
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker-ru.js"></script>
<script src="/js/ui/jquery.ui.core.js"></script>
<script src="/js/ui/jquery.ui.datepicker.js"></script>

<script>
$( document ).ready(function() {
	
	
	$("#img").click(function(){
		$("#file").click();
	})
	
	<? if($mode == 'edit') { ?>
		$("#status_id").val(<?=$auction['status_id']?>);
	<? } ?>
	
	if($("#status_id").val() == 3) {
		$("#save_btn").hide();
		$("select").attr('disabled','disabled');
		$("input, textarea").attr('readonly',true);
	}
	else {
		$('#dt_end').datepicker({
			changeMonth: true,
			changeYear: true
		});	
	}
});

	
	
</script>
<style>
.left_block{
	float:left;
	line-height:21px;	
	width:230px;
	margin-bottom:10px;
	text-align:right;
	padding-right:5px;
}	
.right_block{
	float:left;

	margin-bottom:10px;
	
}
.input, select{
	width:350px;
	
}


textarea{
	resize: none;
}

#img{
	max-width:350px;
	max-height:200px;
	cursor:pointer;
	border:1px solid #69c;
}

#content_file{
	position:absolute;
	left:-9999px;
}
</style>

<h1 class="page-header">
	<?
		if($mode == 'edit') {
			echo "Редактирования аукциона";
		}
	
		elseif($mode=='add') {			
			echo "Добавление аукциона";
		}
	?>
</h1>




<div style="width:620px; margin:0 auto;">
	<form action="" id="form" method="POST" enctype="multipart/form-data">
	
		<div id="content_file">
			<input type="file" id="file" name="file" class="input_fileload">
		</div>
		<input type="hidden" name="action" value="save">
		<input type="hidden" id="delete" name="delete" value="0">
		<input type="hidden" name="id" value="<?=$_GET['id']?>">
		
		<div class="left_block">Статус</div>
		<div class="right_block">
			<select id="status_id" name="status_id">
				<?
					if(!empty($status))
						foreach($status as $item){ ?>
							<option value="<?=$item['id']?>"><?=$item['name']?></option>
						<? }
				?>
			</select>
		</div>
		<div style="clear:both;"></div>
		
		<div class="left_block">Название</div>
		<div class="right_block">
			<input class="form-control input" autocomplete="off"   name="name" type="text" value="<?=($mode != 'add')?$auction['name']:''?>"/>
		</div>
		<div style="clear:both;"></div>
		<div class="left_block">Описание</div>
		<div class="right_block">
			<textarea name="description"  class="form-control input"><?=$auction['description']?></textarea>
		</div>
		<div style="clear:both;"></div>
		
		
		<div class="left_block">Изображение</div>
		<div class="right_block">
			<img id="img" src="/files/auctions/<?=$auction['img']?$auction['img']:'not_img.png'?>" title="Кликните для выбора изображения" />
		</div>
		<div style="clear:both;"></div>
		<div class="left_block">Величина одной ставки</div>
		<div class="right_block">
			<input class="form-control input" autocomplete="off" name="rate_magnitude" type="text" value="<?=($mode != 'add')?$auction['rate_magnitude']:''?>"/>
		</div>
		<div style="clear:both;"></div>
		
		<div class="left_block">Цена моментального выигрыша </div>
		<div class="right_block">
			<input class="form-control input" autocomplete="off" name="price_instant_win" type="text" value="<?=($mode != 'add')?$auction['price_instant_win']:''?>"/>
		</div>
		<div style="clear:both;"></div>
		
		
	
		<div class="left_block">Дата окончания</div>
		<div class="right_block">
			<input class="form-control input" id="dt_end" name="dt_end" value="<?=($mode != 'add')?$auction['dt_end']:''?>"  type="text"/>
			
		</div>
		<div style="clear:both;"></div>
		
		
		<div class="left_block"></div>
		<div class="right_block">
			
			<button id="save_btn" type="button" class="btn btn-default" onclick="$('#form').submit();">Сохранить</button>
		
			<button type="button" onclick="document.location='/admin/auctions'" class="btn btn-default">Закрыть</button>
			
			<? if($mode == 'edit') { ?>
				<button type="button" class="btn btn-default" onclick="$('#delete').val(1); $('#form').submit();">Удалить</button>
			
			<? } ?>
		</div>
		<div style="clear:both;"></div>
		

		
	</form>
</div>