<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();
class Controller_Admin extends Controller
{
	function __construct()
	{
		$this -> model = new Model_Admin();
		$this -> view = new View();
	}
	function index()
	{	
		
		$this->view->generate('admin/index_view.php', 'template_view.php',array());
	}
	
	function auctions() {
		
		
		$this->view->generate('admin/auctions_view.php', 'template_view.php',array());	
	}
	
	function users() {
		
		
		$this->view->generate('admin/users_view.php', 'template_view.php',array());	
	}
	
	function user_delete() {
		$res = $this -> model-> delete_user($_GET['id']);
		if(!$res)
			setMessage('Произошла ошибка удаления','err');
		else	
			setMessage('Пользователь был успешно удален');
			
		headerTo("/admin/users");
	}
	
	function add_auction(){
		if($_POST['action'] == 'save') {
			if(!$this -> model-> add_auction($_POST)) {
				setMessage('Произошла ошибка создания','err');
			}
			else {
				setMessage('Аукцион успешно создан');
			}
			headerTo("/admin/auctions");
		}
		
		$status = $this-> model-> get_auction_status();
		$this->view->generate('admin/edit_auction_view.php', 'template_view.php',array('mode'=>'add','status'=>$status));		
	}
	
	function edit_auction(){
		$auction = $this -> model-> get_auction_by_id($_GET['id']);
		
		if($_POST['action'] == 'save') {
			if($_POST['delete']) {
				if(!$this -> model-> delete_auction($_POST['id'])) {
					setMessage('Произошла ошибка удаления','err');
				}
				else {
					setMessage('Аукцион успешно удален');
				}
			}
			else {
				if(!$this -> model-> update_auction($_POST)) {
					setMessage('Произошла ошибка редактирования','err');
				}
				else {
					setMessage('Аукцион успешно отредактирован');
				}
			}
			headerTo("/admin/auctions");
		}
		$status = $this-> model-> get_auction_status();
		$this->view->generate('admin/edit_auction_view.php', 'template_view.php',array('mode'=>'edit', 'auction'=>$auction,'status'=>$status));		
	
	}
	
}