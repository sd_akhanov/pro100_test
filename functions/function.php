<?php



//переадресация и остановка дальнешего выполнения скрипта
function headerTo($url) {
	header('Location:'.$url);
	exit;
}

//возвращает сообщения после какого то действия
function getMessagesAfterAction()
{
	$string = '';
	
	if (isset($_SESSION['messages']))
	{
		foreach ($_SESSION['messages'] as $class=>$mess)
		{
			foreach ($mess as $m)
				$string .= getMessageError($m, $class);
		}
		unset($_SESSION['messages']);
	}
	if($string != '')
	$string = '<div style="margin-bottom:10px;"><div>'.$string.'</div></div><div style="clear:both;"></div>';

	return $string;
}


function getMessageError($string, $class='err')
{
  $string = trim($string);
  if ($string!='')
    return '<div class="'.$class.'">'.($class=='err'?'Ошибка: ':'').$string.'</div>';
}

function setMessage($string, $class='ok')
{
	global $last_sql_err;
	
	if (!$string && $last_sql_err)
	{
		$string = $last_sql_err;
		$_SESSION['messages']['err'][md5($string)] = $string;
	}
	else {
		$_SESSION['messages'][$class][md5($string)] = $string;
	}	
}




//возвращает данные по пользователю
function get_user_roles($id) {
	$id = (int)$id;	
	$sql = "select u.* from users u where u.id = {$id}";
	$user =  fetch_row(query($sql));
	$sql_roles = "SELECT t.role_id 	FROM users2roles t	WHERE t.user_id =  {$id}";
	$temp = query($sql_roles);
	$rights = array();
	if($temp) {
		while($row = fetch_next($temp)) {
				
			$user['roles'][$row['role_id']] = $row['role_id'];
		}
	}
	
	
	return $user;
}

