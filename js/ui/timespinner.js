/**
 *
 * Extend jQuery.ui.spinner
 * Author: Khisamutdinov Radik
 * 
 */
(function($){
	
	var defaultValue = "00:00",
	
		format = /^(0[0-9]|[0-9]|1[0-9]|2[0-4]):(0[0-9]|[0-9]|[1-5][0-9]|60)$/;
	
	$.widget("ui.timespinner", $.ui.spinner, {

			options: {
				step: 60 * 1000, // seconds
				page: 60, 		 // hours
				change: function () {
					var value = $(this).val();
					if (!format.test(value)) {
						value = defaultValue;	
					}
					$(this).val(value);
				}
			},
			
			_create: function () {			
				var value = this.element.val();
				
				if (value === "" || !format.test(value)) {
					this.element.val(defaultValue);
				}
				
				this._super('_create');
			},
			
			_parse: function (value) {
				if (typeof value === "string") {
					// already a timestamp
					if (Number(value) == value) {
						return Number(value);
					}
					return this.parseDate(value);
				}
				return value;
			},

			_format: function (value) {
				return this.format(new Date(value));
			},
			
			parseDate: function (value) {
				var date = new Date(), time = value.split(':');				
				date.setHours(parseInt(time[0], 10));
				date.setMinutes(parseInt(time[1], 10));
				date.setSeconds(0);
				return date.getTime();
			},
			
			format: function (date) {
				var h, m;				
				h = date.getHours();
				m = date.getMinutes();
				h = h.toString().length > 1 ? h : "0" + h;
				m = m.toString().length > 1 ? m : "0" + m;
				return h + ":" + m;
			}
	});
	
})(jQuery);