<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();
class Controller_Auction extends Controller
{
	function __construct()
	{
		$this -> model = new Model_Auction();
		$this -> view = new View();
	}
	function index()
	{	
		$this->view->generate('auction/auctions_view.php', 'template_view.php',array());
		
	}
	
	function statistic(){
		$this->view->generate('auction/statistic_view.php', 'template_view.php',array());
	}
}