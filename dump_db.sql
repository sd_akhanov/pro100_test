-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 28 2016 г., 10:14
-- Версия сервера: 5.5.24-log
-- Версия PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `auction`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auctions`
--

CREATE TABLE IF NOT EXISTS `auctions` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `status_id` int(1) NOT NULL DEFAULT '1',
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `img` varchar(100) NOT NULL,
  `rate_magnitude` int(15) NOT NULL,
  `price_instant_win` int(15) NOT NULL,
  `dt_end` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `auctions`
--

INSERT INTO `auctions` (`id`, `status_id`, `name`, `description`, `img`, `rate_magnitude`, `price_instant_win`, `dt_end`) VALUES
(1, 1, 'Лампа', 'Лампа', '1.jpg', 15, 15, '2016-07-31'),
(2, 1, 'tes', 'test2', '2.png', 123, 32312, '2016-07-01'),
(3, 3, 'test', 'sdsadas', '2.png', 123, 123123, '2016-07-27'),
(4, 3, 'test', 'test222112', 'jpg', 2323, 23423, '2016-07-28'),
(5, 1, 'sdfsdfsdf', 'sdsfdfsdfsdfsdf', 'jpg', 123123, 12312312, '2016-07-30'),
(6, 1, 'sdfsdf', 'sdfsdfsdf', '6jpg', 123123, 213123, '2016-07-29'),
(7, 1, 'sdas', 'asdasd', '7.jpg', 50, 0, '2016-07-20'),
(9, 1, 'zxc', 'zxc2', '9.jpg', 1, 2, '2016-07-26'),
(10, 3, 'яячяччя', 'ясячсяч', 'вфыв', 123, 12312, '2016-07-28');

-- --------------------------------------------------------

--
-- Структура таблицы `auction_status`
--

CREATE TABLE IF NOT EXISTS `auction_status` (
  `id` int(2) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auction_status`
--

INSERT INTO `auction_status` (`id`, `name`) VALUES
(1, 'Активный'),
(2, 'Отмененный'),
(3, 'Завершенный');

-- --------------------------------------------------------

--
-- Структура таблицы `rates`
--

CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `auction_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `dt` datetime NOT NULL,
  `value` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auction_id` (`auction_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `rates`
--

INSERT INTO `rates` (`id`, `auction_id`, `user_id`, `dt`, `value`) VALUES
(1, 2, 1, '2016-07-26 04:31:06', 123),
(2, 2, 5, '2016-07-28 09:50:19', 246),
(5, 7, 5, '2016-07-28 12:46:12', 50),
(6, 7, 5, '0000-00-00 00:00:00', 100);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(2) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `mail` varchar(50) NOT NULL,
  `pass` char(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `mail`, `pass`) VALUES
(1, 'admin@mail.ru', '21232f297a57a5a743894a0e4a801fc3'),
(5, '1@mail.ru', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Структура таблицы `users2roles`
--

CREATE TABLE IF NOT EXISTS `users2roles` (
  `user_id` int(15) NOT NULL,
  `role_id` int(2) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users2roles`
--

INSERT INTO `users2roles` (`user_id`, `role_id`) VALUES
(1, 1);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auctions`
--
ALTER TABLE `auctions`
  ADD CONSTRAINT `auctions_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `auction_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`),
  ADD CONSTRAINT `rates_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users2roles`
--
ALTER TABLE `users2roles`
  ADD CONSTRAINT `users2roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `users2roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
