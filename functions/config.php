<?php
if(!defined("BASEPATH") || BASEPATH!==true)die();

header('Content-type: text/html; charset=utf-8');
error_reporting (E_ALL&~E_NOTICE);
ini_set('display_errors', 1);

date_default_timezone_set('Europe/Samara');
require_once 'database.php';

connect();


//если идет обращение к директории scripts проверяем авторизован ли пользователь
if(strtolower(substr($_SERVER['REQUEST_URI'],1,7)) == 'scripts' && !isset($d544e2fe9ab7563ac943f6932a571aae)) {
	$flag = false;
	if(isset($_SESSION)){
		$flag = true;
	}
	
	session_start();
	if(!$_SESSION['user'])	
		exit('Авторизуйтесь!!!');
	if(!$flag)
		session_write_close();	
}


