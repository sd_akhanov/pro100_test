<?php
define("BASEPATH", true);	
session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/functions/config.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/functions/function.php';


session_write_close();


$sql = "select t.*, mv.max_value, DATE_FORMAT(t.dt_end,'%d.%m.%Y') as dt_end,

(

	select count(*)  from rates r
inner join auctions a on a.id = r.auction_id
 where r.dt = (select max(r2.dt) from rates r2 where r2.auction_id = r.auction_id) 
 and a.status_id = 3
 and r.user_id = {$_SESSION['user']['id']}
 and r.auction_id = t.id
) as is_win

 from auctions t 
inner join (
select r.auction_id, max(r.value) as max_value from rates r where r.user_id = {$_SESSION['user']['id']} group by r.auction_id) mv on mv.auction_id = t.id
 ";

//echo $sql;exit;
$users = fetch_all_rows(query($sql));

if(!empty($users)) {
foreach($users as $item) { 
	$class_name = "";
	if($item['is_win'])
		$class_name = "is_win";
?>
	<tr class="<?=$class_name?>">
		<td><?=$item['name']?></td>		
		<td><?=$item['description']?></td>
		<td><img src="/files/auctions/<?=$item['img']?>" class="img" title="Кликните на изображение дял увеличения" /></td>
		<td><?=$item['rate_magnitude']?></td>
		<td><?=$item['price_instant_win']?></td>
		<td><?=$item['dt_end']?></td>
		<td class="max_value"><?=$item['max_value']?></td>
		<td><?=$item['is_win']?"Да":"Нет"?></td>
	</tr>
<? } } else { ?>
	<tr ><td colspan="20" align="center">Нет данных</td></tr>
<? }